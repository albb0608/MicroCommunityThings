package com.java110.gateway.sip.callback;

public interface OnProcessListener {
	void onError(String callId);
}
